import os
from pathlib import Path
from pydantic_settings import BaseSettings, SettingsConfigDict
from pydantic import Field

BASE_DIR = Path(__file__).parent.parent


class Settings(BaseSettings):
    api_prefix: str = "/api/v1/notification"
    db_user: str = Field(..., env="DB_USER")
    db_pass: str = Field(..., env="DB_PASS")
    db_host: str = Field(..., env="DB_HOST")
    db_port: str = Field(..., env="DB_PORT")
    db_name: str = Field(..., env="DB_NAME")
    db_echo: bool = True
    auth_api_url: str = "http://fastapi-auth:8000/api/v1/auth"


    model_config = SettingsConfigDict(env_file="C:\\Users\Zend1k\\Desktop\\dyploma\\notification_service\\.env")

    @property
    def db_url(self) -> str:
        return (
            f"mysql+aiomysql://{self.db_user}:{self.db_pass}@"
            f"{self.db_host}:{self.db_port}/{self.db_name}"
        )

    @property
    def db_url_for_alembic(self) -> str:
        return (
            f"mysql+mysqlconnector://{self.db_user}:{self.db_pass}@"
            f"{self.db_host}:{self.db_port}/{self.db_name}"
        )


settings = Settings()
