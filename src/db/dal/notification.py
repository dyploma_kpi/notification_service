from sqlalchemy import select

from src.db import DatabaseHelper, db_helper
from src.models.dal import Notification

class NotificationDAL():

    def __init__(self, db_helper: DatabaseHelper) -> None:
        self._db_helper = db_helper


    async def create_notification(self, notification: Notification) -> Notification:
        async with self._db_helper.session_factory() as session:
            try:
                session.add(notification)
                await session.commit()
                await session.refresh(notification)
                return notification
            except Exception as e:
                print(f"Error create notification: {e}")
                return None

    async def update_notification(self, notification_id: int) -> Notification | None:
        async with self._db_helper.session_factory() as session:
            try:
                stmt = select(Notification).where(Notification.id == notification_id)
                notification: Notification | None = await session.scalar(stmt)
                if not notification:
                    return None

                setattr(notification, "is_open", True)

                await session.commit()
                return notification
            except Exception as e:
                print(f"Error update notification: {e}")
                return None

    async def get_notifications(self, user_id: int) -> list[Notification]:
        async with self._db_helper.session_factory() as session:
            try:
                stmt = select(Notification).where(
                                                    (Notification.user_id == user_id) &
                                                    (Notification.is_open == False)) \
                                           .order_by(Notification.creation_datetime.desc())

                notifications: list[Notification] | None = await session.scalars(stmt)

                await session.commit()
                return notifications
            except Exception as e:
                print(f"Error get notifications: {e}")
                return False


notification_dal = NotificationDAL(db_helper)
