from contextlib import asynccontextmanager
from src.config import settings

from fastapi import (FastAPI,
                     status,
                     Depends,
                     Header)
from fastapi.middleware.cors import CORSMiddleware

from src.service import NotificationService, notification_service

from src.models.schems import (CreateNotification,
                               UpdateNotification,
                               InfoNotification)

def get_notification_service() -> NotificationService:
    return notification_service

@asynccontextmanager
async def lifespan(app: FastAPI):
    yield


app = FastAPI(lifespan=lifespan, openapi_prefix=settings.api_prefix)

origins = [
    "http://localhost:4200",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.post("", status_code=status.HTTP_201_CREATED)
async def create_notification(
    notification: CreateNotification,
    notification_srv: NotificationService = Depends(get_notification_service)
):
    if await notification_srv.create_notification(notification):
        return {"message": "Notification created successfully"}

@app.patch("", status_code=status.HTTP_200_OK)
async def update_notification(
    notification: UpdateNotification,
    auth: str = Header(None),
    notification_srv: NotificationService = Depends(get_notification_service)
):
    if await notification_srv.update_notification(notification, auth):
        return {"message": "Notification updated successfully"}

@app.get("", response_model=list[InfoNotification], status_code=status.HTTP_200_OK)
async def get_profiles(
    auth: str = Header(None),
    notification_srv: NotificationService = Depends(get_notification_service)
):
    return await notification_srv.get_notifications(auth)
