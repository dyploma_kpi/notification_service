from sqlalchemy import String, Integer, Text, Boolean
from sqlalchemy.orm import Mapped, mapped_column

from src.models.dal.base import Base


class Notification(Base):
    user_id: Mapped[int] = mapped_column(Integer, nullable=False)
    text: Mapped[str] = mapped_column(Text, nullable=False)
    url: Mapped[str] = mapped_column(String(300), nullable=False)
    is_open: Mapped[bool] = mapped_column(Boolean, nullable=False, server_default='0')
