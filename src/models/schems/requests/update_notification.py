from typing import Annotated
from pydantic import BaseModel, Field


class UpdateNotification(BaseModel):
    id: Annotated[int, Field(ge=1)]
