from typing import Annotated
from annotated_types import MaxLen
from pydantic import BaseModel, Field


class InfoNotification(BaseModel):
    id: Annotated[int, Field(ge=1)]
    text: Annotated[str, MaxLen(300)]
    url: Annotated[str, MaxLen(300)]
