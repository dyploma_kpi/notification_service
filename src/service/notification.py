import json
from src.db import NotificationDAL, notification_dal
from src.config import settings
from src.models.schems import CreateNotification, UpdateNotification, InfoNotification
from src.models.dal import Notification
from src.utils import Verificator, verificator

from pydantic import parse_obj_as

from fastapi import HTTPException, status


class NotificationService():
    def __init__(self, notification_dal: NotificationDAL, verificator: Verificator) -> None:
        self._notification_dal: NotificationDAL = notification_dal
        self._verificator: Verificator = verificator

    async def create_notification(self, notification: CreateNotification) -> bool:
        if not await self._notification_dal.create_notification(Notification(user_id=notification.user_id, text=notification.text, url=notification.url)):
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR
            )
        return True

    async def update_notification(self, notification: UpdateNotification, token: str) -> bool:
        user_id: int = await self._verificator.verify(token)
        if not await self._notification_dal.update_notification(notification.id):
            raise HTTPException(
                status_code=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )
        return True

    async def get_notifications(self, token: str) -> list[InfoNotification]:
        user_id: int = await self._verificator.verify(token)
        notifications: list[Notification] | None = await self._notification_dal.get_notifications(user_id)
        return parse_obj_as(list[InfoNotification], [vars(notification) for notification in notifications])


notification_service = NotificationService(notification_dal, verificator)
